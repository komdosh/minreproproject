package com.example.ex.service;

import com.example.ex.model.simple.City;
import java.util.List;

public interface CityService {
  City find();

  List<City> findAll();
}
