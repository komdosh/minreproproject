package com.example.ex.service;

import com.example.ex.model.simple.City;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class CityServiceImpl implements CityService {

  @Override
  @Transactional(readOnly = true)
  public City find() {
    City city = new City();
    city.setRegionId(1L);
    city.setCityName("Saint-Petersburg");
    city.setId(0L);
    return city;
  }

  @Override
  @Transactional(readOnly = true)
  public List<City> findAll() {
    City city = new City();
    city.setRegionId(1L);
    city.setCityName("Saint-Petersburg");
    city.setId(0L);
    return Collections.singletonList(city);
  }
}

