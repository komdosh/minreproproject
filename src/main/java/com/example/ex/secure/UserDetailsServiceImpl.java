package com.example.ex.secure;

import com.example.ex.model.simple.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(String name) {
    User user = new User();
    List<String> roles = Collections.singletonList("USER");

    return new CustomUserDetails(user, roles);
  }
}
