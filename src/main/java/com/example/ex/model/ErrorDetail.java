package com.example.ex.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ErrorDetail {
  private int status;

  private String message;

  private String url;
}
