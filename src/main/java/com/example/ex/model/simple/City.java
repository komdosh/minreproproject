package com.example.ex.model.simple;

import com.example.ex.model.basic.AbstractCity;
import lombok.Setter;

@Setter
public class City extends AbstractCity {
  private Long regionId;

  public Long getRegionId() {
    return regionId;
  }
}
