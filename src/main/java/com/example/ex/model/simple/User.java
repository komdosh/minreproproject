package com.example.ex.model.simple;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class User {

  private Long id;
  private String userName;
  private String password;

  public User(Long id, String name, String password) {
    this.id = id;
    this.userName = name;
    this.password = password;
  }
}
