package com.example.ex.model.basic;

import lombok.Setter;

@Setter
public class AbstractCity {
  private Long id;

  private String cityName;

  public Long getId() {
    return id;
  }

  public String getCityName() {
    return cityName;
  }
}
