package com.example.ex.controllers.exceptions;

/**
 * Created by z003nhmz on 14.02.2017.
 */
public class UserVerificationException extends RuntimeException {
  public UserVerificationException(final String message) {
    super(message);
  }
}