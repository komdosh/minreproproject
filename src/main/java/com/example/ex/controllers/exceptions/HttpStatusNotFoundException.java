package com.example.ex.controllers.exceptions;

public class HttpStatusNotFoundException extends RuntimeException {
  public HttpStatusNotFoundException(final String message) {
    super(message);
  }
}
