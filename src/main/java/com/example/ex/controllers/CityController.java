package com.example.ex.controllers;

import com.example.ex.model.simple.City;
import com.example.ex.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseStatus(HttpStatus.OK)
@RequestMapping(value = "/city")
public class CityController {

  @Autowired
  CityService cityService;

  @PreAuthorize("hasAuthority('USER')")
  @RequestMapping(method = RequestMethod.GET, produces = "application/json")
  public City load() {
    return cityService.find();
  }
}
