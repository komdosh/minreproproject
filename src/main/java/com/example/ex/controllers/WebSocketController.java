package com.example.ex.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

  @Autowired
  public SimpMessageSendingOperations messagingTemplate;

  @MessageMapping("/chat")
  public void receiveSendSupportChat() {
    messagingTemplate.convertAndSend("/topic/messages/", "hello websocket!");

  }
}
