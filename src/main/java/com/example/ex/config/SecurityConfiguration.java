package com.example.ex.config;

import com.example.ex.secure.CsrfHeaderFilter;
import com.example.ex.secure.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.session.HttpSessionEventPublisher;


/**
 * Applications's Spring security configuration class.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = "com.example.ex")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserDetailsServiceImpl userDetailsService;

  @Autowired
  public void registerGlobalAuthentication(AuthenticationManagerBuilder auth)
  throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(getShaPasswordEncoder());
  }

  @Bean
  public ShaPasswordEncoder getShaPasswordEncoder() {
    return new ShaPasswordEncoder();
  }

  @Override
  protected void configure(HttpSecurity http)
  throws Exception {
    http
        .authorizeRequests()
        .antMatchers("/static/**", "/**").permitAll()
        .anyRequest().authenticated()
        .and()
        .logout()
        .and()
        .httpBasic()
        .and()
        .sessionManagement()
        .maximumSessions(1)
        .maxSessionsPreventsLogin(false)
        .sessionRegistry(sessionRegistry())
        .and()
        .and()
        .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
        .csrf()
        .ignoringAntMatchers("/app/**", "/chats/**")
        .csrfTokenRepository(csrfTokenRepository())
        .and()
        .headers()
        // allow same origin to frame our site to support iframe SockJS
        .frameOptions()
        .sameOrigin();
  }

  private CsrfTokenRepository csrfTokenRepository() {
    HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
    repository.setHeaderName("X-XSRF-TOKEN");
    return repository;
  }

  @Bean(name = "sessionRegistry")
  public SessionRegistry sessionRegistry() {
    return new SessionRegistryImpl();
  }

  @Bean(name = "authenticationManager")
  @Override
  public AuthenticationManager authenticationManagerBean()
  throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
    return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
  }
}
