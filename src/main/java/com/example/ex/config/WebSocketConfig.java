package com.example.ex.config;

import com.example.ex.config.websocket.interceptors.PresenceChannelInterceptor;
import com.example.ex.config.websocket.interceptors.SubscribeInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.eclipse.jetty.util.DecoratedObjectFactory;
import org.eclipse.jetty.websocket.api.WebSocketBehavior;
import org.eclipse.jetty.websocket.api.WebSocketPolicy;
import org.eclipse.jetty.websocket.server.WebSocketServerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.jetty.JettyRequestUpgradeStrategy;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;
import javax.servlet.ServletContext;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

  @Autowired
  ServletContext servletContext;

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint("/chats")
        .setHandshakeHandler(handshakeHandler())
        .setAllowedOrigins("*")
        .withSockJS().setClientLibraryUrl("//cdn.jsdelivr.net/sockjs/1.1.2/sockjs.min.js");
  }

  @Bean
  public DefaultHandshakeHandler handshakeHandler() {

    WebSocketPolicy policy = new WebSocketPolicy(WebSocketBehavior.SERVER);
    policy.setInputBufferSize(8192);
    policy.setIdleTimeout(TimeUnit.DAYS.toMillis(1));

    servletContext.setAttribute(DecoratedObjectFactory.ATTR, new DecoratedObjectFactory());
    return new DefaultHandshakeHandler(new JettyRequestUpgradeStrategy(new WebSocketServerFactory
        (servletContext, policy)));
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry config) {
    config.enableSimpleBroker("/queue", "/topic");
    config.setApplicationDestinationPrefixes("/app");
  }

  @Override
  protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
    messages.simpDestMatchers("/app/chat/**", "/chat/**")
        .hasAnyAuthority("USER");
  }

  @Override
  protected boolean sameOriginDisabled() {
    return true;
  }

  @Override
  public void configureClientOutboundChannel(ChannelRegistration registration) {
    registration.taskExecutor().corePoolSize(8);
    registration.setInterceptors(presenceChannelInterceptor());
  }

  /**
   * Use for debug of STOMP over WebSocket.
   * @see PresenceChannelInterceptor
   */
  @Bean
  public PresenceChannelInterceptor presenceChannelInterceptor() {
    return new PresenceChannelInterceptor();
  }

  @Override
  protected void customizeClientInboundChannel(final ChannelRegistration registration) {
    registration.setInterceptors(new SubscribeInterceptor());
    registration.setInterceptors(presenceChannelInterceptor());
  }

  @Override
  public boolean configureMessageConverters(List<MessageConverter> messageConverters) {
    messageConverters.add(new GsonMessageConverter());
    return true;
  }

  private static class GsonMessageConverter implements MessageConverter {

    Gson gson;

    public GsonMessageConverter() {
      gson = new GsonBuilder().create();
    }

    @Override
    public Object fromMessage(Message<?> message, Class<?> aClass) {
      try {
        return gson.fromJson(new String(((byte[]) (message.getPayload())), "UTF-8"), aClass);
      } catch (UnsupportedEncodingException e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public Message<?> toMessage(Object o, MessageHeaders messageHeaders) {
      return new GenericMessage<>(gson.toJson(o).getBytes(Charset.forName("UTF-8")), messageHeaders);
    }
  }
}
