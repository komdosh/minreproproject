package com.example.ex.config.websocket.interceptors;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import java.util.Arrays;

/**
 * {@link org.springframework.messaging.support.ChannelInterceptor} for debugging
 * and logging messaging with client and server.
 */
public class PresenceChannelInterceptor extends ChannelInterceptorAdapter {

  @Override
  public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
    StompHeaderAccessor sha = StompHeaderAccessor.wrap(message);

    if (sha.getCommand() != null) {
      System.err.println("STOMP " + sha.getCommand().toString() + " "
          + Arrays.toString(sha.toMap().entrySet().toArray()));
    }
  }
}
