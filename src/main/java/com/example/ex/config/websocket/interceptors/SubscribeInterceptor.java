package com.example.ex.config.websocket.interceptors;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

/**
 * {@link org.springframework.messaging.support.ChannelInterceptor} for debugging
 * and logging messaging with client and server.
 */
public class SubscribeInterceptor extends ChannelInterceptorAdapter {

  @Override
  public Message<?> preSend(Message<?> message, MessageChannel channel) {
    return message;
  }


}
