package com.example.ex.config;

import com.example.ex.controllers.exceptions.HttpStatusNotFoundException;
import com.example.ex.controllers.exceptions.UserVerificationException;
import com.example.ex.model.ErrorDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class GlobalControllerExceptionHandler {

  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ExceptionHandler({AuthenticationException.class, AccessDeniedException.class,
      BadCredentialsException.class, UserVerificationException.class})
  @ResponseBody
  public ErrorDetail handleAccessDeniedException(Exception e, HttpServletRequest req) {
    ErrorDetail error = new ErrorDetail();
    error.setStatus(HttpStatus.FORBIDDEN.value());
    error.setUrl(req.getRequestURL().toString());
    log.error("{}", error);
    return error;
  }

  @ResponseBody
  @SuppressWarnings("squid:S1148")
  @ExceptionHandler(value = Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorDetail defaultErrorHandler(HttpServletRequest req, Exception e) {
    e.printStackTrace();
    log.error("{}", e);
    ErrorDetail error = new ErrorDetail();
    error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    error.setMessage(e.getMessage());
    error.setUrl(req.getRequestURL().toString());
    return error;
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = MethodArgumentNotValidException.class)
  public ErrorDetail validationFailedException(HttpServletRequest req, Exception e) {
    log.error("Validation exception", e);
    final ErrorDetail errorDetail = new ErrorDetail();
    errorDetail.setStatus(HttpStatus.BAD_REQUEST.value());
    errorDetail.setMessage("validation failed");
    errorDetail.setUrl(req.getRequestURL().toString());
    return errorDetail;
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = IllegalArgumentException.class)
  public ErrorDetail illegalArgumentException(HttpServletRequest req, Exception e) {
    log.error("Illegal Argument Exception", e);
    final ErrorDetail errorDetail = new ErrorDetail();
    errorDetail.setStatus(HttpStatus.BAD_REQUEST.value());
    errorDetail.setMessage(e.getMessage());
    errorDetail.setUrl(req.getRequestURL().toString());
    return errorDetail;
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(value = HttpStatusNotFoundException.class)
  public ErrorDetail httpStatusNotFoundExceptionHandler(HttpServletRequest req, Exception e) {
    log.error("{}", e);
    ErrorDetail error = new ErrorDetail();
    error.setStatus(HttpStatus.NOT_FOUND.value());
    error.setMessage(e.getMessage());
    error.setUrl(req.getRequestURL().toString());
    return error;
  }
}
