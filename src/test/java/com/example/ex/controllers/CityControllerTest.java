package com.example.ex.controllers;

import com.example.ex.config.WebAppConfig;
import com.example.ex.model.simple.City;
import com.example.ex.service.CityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppConfig.class})
@WebAppConfiguration
public class CityControllerTest {

  private MockMvc mockMvc;

  @InjectMocks
  private CityController cityController;

  @Mock
  private CityService cityService;

  @Test
  public void load()
  throws Exception {
    City city = new City();
    city.setRegionId(1L);
    city.setCityName("Saint-Petersburg");
    city.setId(0L);
    when(cityService.find()).thenReturn(city);

    mockMvc.perform(get("/city", 1L))
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json;charset=UTF-8"))
        .andExpect(jsonPath("cityName").value("Saint-Petersburg"))
        .andExpect(jsonPath("id").value(0))
        .andExpect(jsonPath("regionId").value(1));
  }


  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    this.mockMvc = MockMvcBuilders.standaloneSetup(cityController).build();
  }
}
